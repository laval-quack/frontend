import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class ChatService {
  private apiEndpoint = `${environment.backendUrl}/chat`;

  constructor(
    private zone: NgZone
  ) { }

  public listen(): Observable<MessageEvent> {
    return new Observable(observer => {
      const eventSource = new EventSource(this.apiEndpoint);

      eventSource.onmessage = message => this.zone.run(() => observer.next(message));
    });
  }

  public send(message: ChatMessage) {
    fetch(this.apiEndpoint, {
      method: 'POST',
      headers: { 'content-type': 'application/json' },
      body: JSON.stringify(message),
    });
  }
}
