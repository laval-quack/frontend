import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'chatDisplay'
})
export class ChatDisplayPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value) {
      const chat: ChatMessage = JSON.parse(value);
      return `[${new Date(chat.timestamp).toLocaleTimeString()}] ${chat.username} - ${chat.message}`;
    }
    return null;
  }

}
