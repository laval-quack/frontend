import { Component, OnInit } from '@angular/core';
import { ChatService } from '../services/chat-service';

@Component({
  selector: 'app-chat',
  template: `
    <div>
      <form (ngSubmit)="submit()">
        <div>
          Your username:
          <input name="username" [(ngModel)]="username" />
        </div>
        <div>
          Quackstream:
          <div *ngFor="let entry of entries">
            {{ entry | chatDisplay }}
          </div>
        </div>
        <div>
          <input name="message" [(ngModel)]="message" />
          <button type="submit">Quack!</button>
        </div>
      </form>
    </div>
  `,
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  public username = '';
  public message = '';
  public entries: string[] = [];

  constructor(
    private chatService: ChatService,
  ) { }

  ngOnInit() {
    this.chatService.listen().subscribe(messageEvent => {
      this.entries.push(messageEvent.data);
    });
  }

  submit() {
    this.chatService.send({
      username: this.username,
      message: this.message,
      timestamp: Date.now(),
    });
  }
}
